//
// Created by jason on 4/17/13.
//
//


#import <Foundation/Foundation.h>


@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UIPanGestureRecognizer *pullDownGestureRecognizer;
- (void)hideContentView;
@end